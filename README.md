# Yelb Demo

[[_TOC_]]

For those not familiar, Yelb is a famous VMware demo application that was built by [Massimo Re Ferre][MassimoReFerre_link] and [Andrea Siviero][Andrea Siviero_link]. This web application contains the following services: UI Frontend, Application Server, Database Server and Caching Service using Redis.

![yelb-lb](./yelb-lb.png)

[MassimoReFerre_link]: https://twitter.com/mreferre
[Andrea Siviero_link]: https://www.linkedin.com/in/andreasiviero/

## Special Requirements

- None

## NodePort Deployment

### Step 1 - Deploy the application
```bash
kubectl create ns yelb
kubectl apply -f https://raw.githubusercontent.com/lamw/vmware-k8s-app-demo/master/yelb.yaml
```

### Step 2 - Verify all pods are ready
```bash
kubectl -n yelb get pods
```

### Step 3 - To access the application, open web browser to http://<ip>:30001
```bash
kubectl -n yelb describe pod $(kubectl -n yelb get pods | grep yelb-ui | awk '{print $1}') | grep "Node:"
```

## LoadBalancer Service Deployment

### Step 1 - Deploy the application
```bash
kubectl create ns yelb
kubectl apply -f https://raw.githubusercontent.com/lamw/vmware-k8s-app-demo/master/yelb-lb.yaml
```

### Step 2 - Verify all pods are ready
```bash
kubectl -n yelb get pods
```

### Step 3 - To access the application, open web browser to http://<external-ip>
```bash
kubectl -n yelb get svc/yelb-ui
```

**Note:** It is expected that your K8s cluster can support a LoadBalancer Service.